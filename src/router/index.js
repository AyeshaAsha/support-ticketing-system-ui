import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/:uname",
    name: "User",
    props: true,
    component: () =>
      import(/* webpackChunkName: "user" */ "../views/User.vue"),
    children: [
      {
        path: "my-profile",
        name: "MyProfile",
        component: () =>
          import("@/components/MyProfile.vue"),
      },
      {
        path: "ticket-info",
        name: "TicketInfo",
        component: () =>
          import("@/components/TicketInfo.vue"),
      },
      {
        path: "new-ticket",
        name: "TicketForm",
        component: () =>
          import("@/components/TicketForm.vue"),
      },
      {
        path: "user-info",
        name: "UserInfo",
        component: () =>
          import("@/components/UserInfo.vue"),
      },
      {
        path: "new-user",
        name: "RegisterForm",
        component: () =>
          import("@/components/RegisterForm.vue"),
      },
      {
        path: "logout",
        name: "Logout",
        component: () =>
          import("@/components/Logout.vue"),
      }
    ]
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
